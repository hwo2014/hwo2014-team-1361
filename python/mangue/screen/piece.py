# encoding: utf-8

import math
import pygame

from mangue.vector import Vector

PIECE_COLOR = pygame.Color('gray')
SWITCH_COLOR = pygame.Color('white')
LANE_COLOR = pygame.Color('black')

LANE_WIDTH = 1

DEGREE = math.radians(1)


class ScreenPiece(object):
    @staticmethod
    def set_lanes(lanes):
        ScreenPiece.lanes = lanes

    def lane_dist(self, lane):
        return lane.dist

    def __init__(self, switch):
        self.switch = switch

    def color(self):
        if self.switch:
            return SWITCH_COLOR
        else:
            return PIECE_COLOR


class ScreenStraightPiece(ScreenPiece):
    def __init__(self, pos, length, direction, **kwargs):
        ScreenPiece.__init__(self, **kwargs)

        self.pos = pos
        self.length = length
        self.direction = direction

    def translate(self, vector):
        self.pos.translate(vector, in_place=True)

    def draw(self, surface, width, lanes):
        width_compensation = Vector(width / 2, width / 2)

        startpos = self.pos.add(width_compensation)
        endpos = self.pos.add(self.direction).add(width_compensation)
        color = self.color()

        pygame.draw.line(surface, color, startpos.to_tuple(), endpos.to_tuple(),
            width)

        normal = self.direction.rotate(90)

        for lane in lanes:
            lanepos = normal.mag(lane.dist)

            start = startpos.add(lanepos)
            end = endpos.add(lanepos)

            pygame.draw.line(surface, LANE_COLOR, start.to_tuple(),
                end.to_tuple(), LANE_WIDTH)

    def get_pos_and_direction(self, start_lane, end_lane, where):
        direction = self.direction.unit()

        if start_lane == end_lane:
            where /= self.length

            lane_normal = self.direction.rotate(90).mag(
                self.lane_dist(start_lane))
        else:
            width = abs(start_lane.dist) + abs(end_lane.dist)
            real_length = math.sqrt(self.length**2 + width**2)
            where /= real_length

            factor = where * end_lane.dist + (1 - where) * start_lane.dist
            lane_normal = self.direction.unit().rotate(90).mult(factor)

            angle = math.degrees(math.atan(width / self.length))
            if start_lane.dist > end_lane.dist:
                angle *= -1
            direction.rotate(angle, in_place=True)

        pos = self.pos.add(self.direction.mult(where)).add(lane_normal)

        return pos, direction

    def __repr__(self):
        return '<Straight [screen] pos: %r direction: %r>' % (self.pos,
            self.direction)


class ScreenBendPiece(ScreenPiece):
    def __init__(self, center, radius, turn, start_angle_ccw, var_angle,
            **kwargs):
        ScreenPiece.__init__(self, **kwargs)

        self.center = center
        self.radius = radius
        self.turn = turn

        self.start_angle_ccw = start_angle_ccw
        self.start_angle_cw = 360 - start_angle_ccw
        self.var_angle = var_angle

    def translate(self, vector):
        self.center.translate(vector, in_place=True)

    def draw(self, surface, width, lanes):
        width_compensation = Vector(width / 2, width / 2)

        center = self.center.add(width_compensation).to_tuple()
        radius = self.radius + width / 2

        rect = self.get_rect(center, radius)
        color = self.color()

        start = math.radians(self.start_angle_ccw)
        end = math.radians(self.start_angle_ccw - self.var_angle)

        if end < start:
            start, end = end, start

        start -= 2 * DEGREE
        end += 2 * DEGREE

        pygame.draw.arc(surface, color, rect, start, end, width)

        for lane in lanes:
            lane_radius = self.radius + lane.dist
            lane_rect = self.get_rect(center, lane_radius)

            pygame.draw.arc(surface, LANE_COLOR, lane_rect, start, end,
                LANE_WIDTH)

    @staticmethod
    def get_rect(center, radius):
        x = center[0] - radius
        y = center[1] - radius
        size = 2 * radius
        return pygame.Rect(x, y, size, size)

    def lane_dist(self, start_lane, end_lane, where):
        if start_lane == end_lane:
            dist = start_lane.dist
        else:
            dist = where * end_lane.dist + (1 - where) * start_lane.dist

        return -dist * math.copysign(1, self.var_angle)

    def get_pos_and_direction(self, start_lane, end_lane, where):
        # FIXME
        length = math.radians(abs(self.var_angle)) * (self.radius
            + self.lane_dist(start_lane, end_lane, 0.5))
        where /= length

        radius = self.radius + self.lane_dist(start_lane, end_lane, where)

        end_angle = self.start_angle_cw + self.var_angle
        angle = (1 - where) * self.start_angle_cw + where * end_angle

        vector = Vector(radius, 0).rotate(angle)
        pos = self.center.add(vector)
        direction = vector.rotate(math.copysign(90, self.var_angle)).unit()

        return pos, direction

    def __repr__(self):
        return '<Bend [screen] center: %r radius: %f %f->%f >' % (self.center,
            self.radius, self.start_angle_cw, self.var_angle)
