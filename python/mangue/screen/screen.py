# encoding: utf-8

import math
import pygame

from mangue.util import Bundle
from mangue.vector import Vector
from mangue.piece import StraightPiece, BendPiece

from mangue.screen.piece import ScreenPiece, ScreenStraightPiece, \
    ScreenBendPiece
from mangue.screen.car import ScreenCar

COLOR_TRANSPARENT = pygame.Color(0, 0, 0, 0)


class Screen(object):
    def __init__(self):
        pygame.init()

        self.pieces = []
        self.cars = []

        self.margin = 25
        self.margin_vector = Vector(self.margin, self.margin)

    def process_track(self, track):
        '''
        * Define a largura da pista baseado na quantidade de lanes
        * Descobre o tamanho da pista (bounding box)
        * Cria as representações das peças
        * Cria as representações dos carros
        * Desenha a pista
        '''

        self.track = track
        self.process_track_width(track.lanes)

        state = {
            'upper_left': Vector(0, 0),
            'lower_right': Vector(0, 0),
            'pos': Vector(0, 0),
            'angle': 0.0
        }

        state = Bundle(**state)

        self.process_pieces(track.pieces, track.lanes, state)
        self.process_cars(track.cars)

        self.create_surfaces(state)
        self.draw_track()

    def process_track_width(self, lanes):
        '''
        A largura da pista é a soma absoluta das distâncias de todas as lanes.

        As lanes externas (a mais a direita e mais a esquerda) são consideradas
        duas vezes.
        '''
        tmp = reduce(lambda acc, lane: acc + abs(lane.dist), lanes, 0)
        tmp += abs(lanes[0].dist) + abs(lanes[-1].dist)

        self.track_width = tmp

    def process_pieces(self, pieces, lanes, state):
        ScreenPiece.set_lanes(lanes)

        for piece in pieces:
            if isinstance(piece, StraightPiece):
                self.process_straight(piece, state)
            else:
                self.process_bend(piece, state)
            self.adjust_bounding_box(state)

        # Translação para remover coordenadas negativas
        translation_vector = Vector(-state.upper_left.x, -state.upper_left.y)

        for piece in self.pieces:
            piece.translate(translation_vector)

    def process_straight(self, piece, state):
        '''
        Cria uma versão "pintável" de uma reta, além de atualizar o estado da
        bounding box.
        '''
        direction = Vector(piece.length, 0).rotate(state.angle)

        kwargs = {
            'pos': state.pos.clone(),
            'length': piece.length,
            'direction': direction,
            'switch': piece.switch,
        }

        self.pieces.append(ScreenStraightPiece(**kwargs))

        state.pos.add(direction, in_place=True)

    def process_bend(self, piece, state):
        '''
        Cria uma versão "pintável" de uma curva, além de atualizar o estado da
        bounding box.
        '''

        # Ângulo de rotação de um vetor para encontrar o centro da curva. Se o
        # ângulo da curva é negativo, então a curva é para esquerda
        center_angle = state.angle + math.copysign(90, piece.angle)

        # Rotaciona um vetor a partir da origem até encontrar o centro da curva
        center_vector = Vector(piece.radius, 0).rotate(center_angle)
        center = state.pos.add(center_vector)

        # Os vetores inicial e final indicam as posições inicial e final da
        # curva
        start_vector = center_vector.mult(-1)
        end_vector = start_vector.rotate(piece.angle)

        # Encontra o ponto final da curva
        state.pos = center.add(end_vector)

        # Encontra o ângulo final somando o ângulo acumulado com o ângulo da
        # curva atual
        state.angle += piece.angle

        vector = Vector(1, 0)

        # O ângulo inicial da curva é o ângulo do start_vector
        start_angle_ccw = vector.angle(start_vector, direction='ccw')

        kwargs = {
            'switch': piece.switch,
            'center': center,
            'radius': piece.radius,
            'turn': piece.turn,

            'start_angle_ccw': start_angle_ccw,
            'var_angle': piece.angle,
        }

        self.pieces.append(ScreenBendPiece(**kwargs))

    def adjust_bounding_box(self, state):
        '''
        1    2    3
          _______
          |UL   |
        4 |  9  | 5
          |___LR|

        6    7    8
        '''

        UL = state.upper_left
        LR = state.lower_right
        pos = state.pos

        # First check if pos is inside bounding box (region 9)
        if pos.x >= UL.x and pos.x <= LR.x and pos.y >= UL.y and pos.y <= LR.y:
            return

        # First test corners
        # Region 1
        if pos.x <= UL.x and pos.y <= UL.y:
            UL.x = pos.x
            UL.y = pos.y

        # Region 8
        elif pos.x >= LR.x and pos.y >= LR.y:
            LR.x = pos.x
            LR.y = pos.y

        # Region 3
        elif pos.x > LR.x and pos.y < UL.y:
            LR.x = pos.x
            UL.y = pos.y

        # Region 6
        elif pos.x < UL.x and pos.y > LR.y:
            UL.x = pos.x
            LR.y = pos.y

        # Region 2 (*condition smaller because of previous tests)
        elif pos.y < UL.y:
            UL.y = pos.y

        # Region 4 (*)
        elif pos.x < UL.x:
            UL.x = pos.x

        # Region 5 (*)
        elif pos.x > LR.x:
            LR.x = pos.x

        #Region 7 (*)
        elif pos.y > LR.y:
            LR.y = pos.y

        else:
            raise Exception('')

    def process_cars(self, cars):
        ScreenCar.set_pieces(self.pieces)

        for car in cars.itervalues():
            self.cars.append(ScreenCar.from_car(car))

    def create_surfaces(self, state):
        track_size = (
            int(state.lower_right.x - state.upper_left.x + self.track_width),
            int(state.lower_right.y - state.upper_left.y + self.track_width)
        )

        screen_size = tuple(int(elem + 2 * self.margin) for elem in track_size)

        self.track_surface = pygame.Surface(track_size)
        self.car_surface = pygame.Surface(track_size, pygame.SRCALPHA, 32)
        self.screen = pygame.display.set_mode(screen_size)

    def draw_track(self):
        for piece in self.pieces:
            piece.draw(self.track_surface, self.track_width, self.track.lanes)

    def draw(self, track):
        '''
        Copia o desenho da pista para a tela e em seguida pinta os carros.
        '''
        self.screen.blit(self.track_surface, self.margin_vector.to_tuple())
        self.car_surface.fill(COLOR_TRANSPARENT)

        for car in self.cars:
            car.draw(self.car_surface, self.track_width)

        self.screen.blit(self.car_surface, self.margin_vector.to_tuple())
        pygame.display.flip()
