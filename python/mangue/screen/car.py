# encoding: utf-8

import pygame

from mangue.vector import Vector

ANCHOR_COLOR = pygame.Color('black')
ANCHOR_RADIUS = 2

class ScreenCar(object):
    @classmethod
    def set_pieces(cls, pieces):
        cls._pieces = pieces

    def __init__(self, length, width, anchor, color, car):
        self.length = length
        self.width = int(width)
        self.anchor = anchor
        self.back_dist = length - anchor
        self.color = pygame.Color(str(color))
        self._car = car

    @staticmethod
    def from_car(car):
        return ScreenCar(car.dimensions.length, car.dimensions.width,
            car.dimensions.anchor, car.color, car)

    # Atributos variáveis
    @property
    def piece(self):
        return self._pieces[self._car.piece.idx]

    @property
    def dist(self):
        return self._car.dist

    @property
    def angle(self):
        return self._car.angle  

    @property
    def start_lane(self):
        return self._car.start_lane

    @property
    def end_lane(self):
        return self._car.end_lane

    @property
    def lap(self):
        return self._car.lap

    def poly(self, pos, direction):
        '''
        Retorna os quatro pontos que formam o polígono do carro.
        '''

        normal1 = direction.rotate(90).mult(self.width / 2)
        normal2 = normal1.mult(-1)

        back = pos.slide(direction, -self.back_dist)
        front = back.add(direction.mult(self.length))

        b1 = back.add(normal1)
        b2 = back.add(normal2)

        f1 = front.add(normal1)
        f2 = front.add(normal2)

        return [b1, b2, f2, f1]

    def draw(self, surface, width):
        width_compensation = Vector(width / 2, width / 2)

        piece_pos, piece_direction = self.piece.get_pos_and_direction(
            self.start_lane, self.end_lane, self.dist)

        # A direção do carro será a tangente da curva rotacionada em relação
        # ao ângulo do carro
        piece_direction.rotate(self.angle, in_place=True)

        points = self.poly(piece_pos, piece_direction)
        
        anchor = piece_pos.add(width_compensation).to_tuple()
        points = [point.add(width_compensation).to_tuple() for point in points]

        pygame.draw.polygon(surface, self.color, points, 0)

        pygame.draw.circle(surface, ANCHOR_COLOR, anchor, ANCHOR_RADIUS, 0)

    def __repr__(self):
        return '<Car [screen] lap: %d piece: %d dist: %f angle: %f>' % (
            self.lap, self._car.piece, self.dist, self.angle)
