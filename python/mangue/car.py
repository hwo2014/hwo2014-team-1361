# encoding: utf-8

from datetime import datetime

from mangue.vector import Vector
from mangue.util import Bundle

__all__ = ['Turbo', 'Car', 'OurCar', 'RivalCar']


class Turbo(object):
    CHARGING = 'CHARGING'
    AVAILABLE = 'AVAILABLE'
    USE = 'USE'


class Car(object):
    @staticmethod
    def set_track(track):
        Car.track = track

    @staticmethod
    def is_our_car(data, name, color):
        return data['id']['name'] == name and data['id']['color'] == color

    def __init__(self, name, color, dimensions):
        self.name = name
        self.color = str(color)

        dimensions = {
            'length': dimensions['length'],
            'width': dimensions['width'],
            'anchor': dimensions['guideFlagPosition']
        }

        self.dimensions = Bundle(**dimensions)

        self.angle = 0.0
        self.angle_ = 0.0
        self.angle__ = 0.0

        self.piece = 0
        self.dist = 0
        self.start_lane = None
        self.end_lane = None
        self.lap = 0

        self._throttle = 0
        self.turbo = Turbo.CHARGING

        self.last_pos = self.curr_pos = None
        self.speed = 0

    @classmethod
    def from_dict(cls, data):
        return cls(data['id']['name'], data['id']['color'], data['dimensions'])

    @property
    def throttle(self):
        return self._throttle

    @property
    def turbo(self):
        return self._turbo

    @property
    def force(self):
        if not self.piece or not self.piece.radius:
            return 0
        else:
            return (self.speed ** 2) / (self.piece.radius
                + self.piece.lane_radius(self.start_lane))

    def update_position(self, data, tick):
        piece_info = data['piecePosition']

        angle = data['angle']
        angle_ = angle - self.angle

        self.angle__ = angle_ - self.angle_
        self.angle_ = angle_
        self.angle = angle

        self.piece = self.track.pieces[piece_info['pieceIndex']]
        self.dist = piece_info['inPieceDistance']
        self.start_lane = self.track.lanes[piece_info['lane']['startLaneIndex']]
        self.end_lane = self.track.lanes[piece_info['lane']['endLaneIndex']]
        self.lap = piece_info['lap']

        self.process_speed(tick)

    def process_speed(self, tick):
        self.last_pos = self.curr_pos
        self.curr_pos = Bundle(piece=self.piece, dist=self.dist, tick=tick)

        not_valid_last = self.last_pos is None or self.last_pos.tick is None
        not_valid_curr = self.curr_pos is None or self.curr_pos.tick is None

        if not_valid_last or not_valid_curr:
            return
        elif self.curr_pos.piece == self.last_pos.piece:
            total_dist = self.curr_pos.dist - self.last_pos.dist
        else:
            last_length = self.last_pos.piece.lane_length(self.start_lane)
            total_dist = last_length - self.last_pos.dist
            total_dist += self.curr_pos.dist

            # Pegar o miolo
            last_idx = self.last_pos.piece.idx
            curr_idx = self.curr_pos.piece.idx
            for idx in xrange(last_idx + 1, curr_idx - 1):
                piece = self.track.pieces[idx]
                total_dist += piece.lane_length(self.start_lane)

        dticks = self.curr_pos.tick - self.last_pos.tick
        self.speed = total_dist / dticks

    def __repr__(self):
        return '<%s %s lap: %d piece: %d lanes: %r, dist: %f angle: %f>' % (
            self.__class__.__name__, self.name, self.lap, self.piece.idx,
            [self.start_lane.idx, self.end_lane.idx], self.dist, self.angle)


class OurCar(Car):
    @Car.throttle.setter
    def throttle(self, value):
        self._throttle = value

    @Car.turbo.setter
    def turbo(self, value):
        self._turbo = value


class RivalCar(Car):
    pass
