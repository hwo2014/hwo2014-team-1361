# encoding: utf-8

import math


class Vector(object):
    def __init__(self, x, y=None):
        if hasattr(x, 'x') and hasattr(x, 'y') and y is None:
            self.x = x.x
            self.y = x.y
        elif y is None:
            raise AttributeError('y is None')
        else:
            self.x = x
            self.y = y

    def norm(self):
        return math.sqrt(self.x ** 2 + self.y ** 2)

    def unit(self, in_place=False):
        norm = self.norm()
        x = self.x / norm
        y = self.y / norm

        if not in_place:
            return Vector(x, y)

        self.x = x
        self.y = y

    def mag(self, value, in_place=False):
        if not in_place:
            unit = self.unit()
            unit.x *= value
            unit.y *= value
            return unit

        unit = self.unit()

        self.x = unit.x * value
        self.y = unit.y * value

    def rotate(self, angle, radians=False, in_place=False):
        if not radians:
            angle = math.radians(angle)

        x = self.x * math.cos(angle) - self.y * math.sin(angle)
        y = self.x * math.sin(angle) + self.y * math.cos(angle)

        if not in_place:
            return Vector(x, y)

        self.x = x
        self.y = y

    def add(self, other, in_place=False):
        if not in_place:
            return Vector(self.x + other.x, self.y + other.y)

        self.x += other.x
        self.y += other.y

    def sub(self, other):
        return Vector(self.x - other.x, self.y - other.y)

    def translate(self, vector, in_place=False):
        return self.add(vector, in_place=in_place)

    def scale(self, factor):
        return Vector(self.x * factor.x, self.y * factor.y)

    def dot(self, other):
        return self.x * other.x + self.y * other.y

    def angle(self, other, direction=None):
        dot = self.dot(other)
        norm = self.norm() * other.norm()
        angle = math.degrees(math.acos(dot / norm))

        if direction is None or direction not in ['cw', 'ccw']:
            return angle

        side = self.side(other)

        if direction == side:
            return angle

        return 360 - angle


    def mult(self, factor, in_place=False):
        x = self.x * factor
        y = self.y * factor

        if not in_place:
            return Vector(x, y)

        self.x = x
        self.y = y

    def slide(self, direction, value, in_place=False):
        vector = direction.mag(value)

        return self.add(vector, in_place=in_place)

    def side(self, other):
        '''
        Verifica em que "lado" o vetor other está considerando a linha que é
        criada pelo vetor atual.

        Utiliza parte do cross product (a componente Z). Se a componente for
        positiva, então o outro vetor está a DIREITA, caso contrário, está a
        ESQUERDA.

        Aparentemente os lados estão trocados (quando se olha pra regra da mão
        direita), mas o eixo y é invertido.
        '''

        z = self.x * other.y - self.y * other.x

        if z >= 0:
            return 'cw'
        else:
            return 'ccw'

    def to_tuple(self):
        return (int(self.x), int(self.y))

    def clone(self):
        return Vector(self)

    def __repr__(self):
        return '({:.2f}, {:.2f})'.format(self.x, self.y)
