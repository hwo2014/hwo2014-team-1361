# encoding: utf-8

from mangue.segment import *
from mangue.piece import *
from mangue.car import *
from mangue.lane import Lane
from mangue.vector import Vector

from mangue.util import Bundle


class Track(object):
    def __init__(self, name):
        self.name = name

        self.pieces = []
        self.lanes = []
        self.cars = {}

        self.segments = []
        self.curr_segment = None

    @staticmethod
    def from_dict(data, bot):
        track = data['race']['track']
        cars = data['race']['cars']
        session = data['race']['raceSession']

        kwargs = {
            'name': track['name']
        }

        track_instance = Track(**kwargs)

        kwargs = {
            'pieces': track['pieces'],
            'lanes': track['lanes'],
            'cars': cars,
            'bot_name': bot.name,
            'bot_color': bot.color
        }

        car = track_instance.process_track(**kwargs)

        return track_instance, car

    def process_track(self, pieces, lanes, cars, bot_name, bot_color):
        Segment.set_track(self)
        Piece.set_track(self)
        Car.set_track(self)

        for idx, data in enumerate(lanes):
            self.lanes.append(Lane.from_dict(data, idx))

        for idx, data in enumerate(pieces):
            if 'length' in data:
                piece = StraightPiece.from_dict(data, idx)
            else:
                piece = BendPiece.from_dict(data, idx)

            self.add_piece(piece)

        self.merge_incomplete_segments()

        our_car = None

        for data in cars:
            if Car.is_our_car(data, bot_name, bot_color):
                car = OurCar.from_dict(data)
                our_car = car
            else:
                car = RivalCar.from_dict(data)

            self.cars[car.name] = car

        return our_car

    def add_piece(self, piece):
        idx = len(self.segments)

        if not idx or piece.switch:
            self.segments.append(Segment(idx))

        self.pieces.append(piece)
        self.segments[-1].add_piece(piece)

    def merge_incomplete_segments(self):
        if not len(self.segments):
            return

        if self.segments[0].is_complete():
            return

        # Prependa as pieces do último segmento na frente do primeiro segmento
        # e remove o último segmento
        self.segments[0].merge(self.segments[-1])
        del self.segments[-1]

    def update_car_positions(self, cars, tick):
        for data in cars:
            name = data['id']['name']
            self.cars[name].update_position(data, tick)
