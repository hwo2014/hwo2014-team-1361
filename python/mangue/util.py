# encoding: utf-8

class Bundle(object):
    def __init__(self, **kwargs):
        for key, value in kwargs.iteritems():
            setattr(self, key, value)

    def __repr__(self):
        content = ', '.join(str(key) + '=' + str(value)
            for key, value in self.__dict__.iteritems())
        return 'Bundle(%s)' % content
