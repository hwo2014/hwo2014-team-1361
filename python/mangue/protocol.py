# encoding: utf-8

import json
import socket
from datetime import datetime

class HWOProtocol(object):
    def __init__(self, host, port):
        self.host = host
        self.port = int(port)
        self.handler = None
        self.started = False

        self.previous_tick_ts = self.curr_tick_ts = None

    def connect(self):
        if self.started:
            raise Exception('Already connected')

        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.connect((self.host, self.port))
        self.file = self.sock.makefile()
        self.started = True

    def set_handler(self, handler):
        self.handler = handler

    @property
    def tick_rate(self):
        if self.previous_tick_ts is None or self.curr_tick_ts is None:
            return 0

        tdelta = self.curr_tick_ts - self.previous_tick_ts

        return 1.0 / tdelta.total_seconds()

    def recv(self):
        if not self.started:
            raise Exception('Not connected')

        raw = self.file.readline()

        if not len(raw):
            return None, None

        msg = json.loads(raw)
        tick = msg.get('gameTick', None)

        if tick is not None:
            self.previous_tick_ts = self.curr_tick_ts
            self.curr_tick_ts = datetime.now()

        return msg['msgType'], msg['data'], tick

    def _send(self, msg_type, data=None, tick=None):
        if not self.started:
            raise Exception('Not connected')

        msg = { 'msgType': msg_type }

        if data is not None:
            msg['data'] = data

        if tick is not None:
            msg['gameTick'] = tick

        self.sock.sendall(json.dumps(msg) + '\n')

    ########## MESSAGES ##########

    def join(self, name, key):
        self._send('join', {
            'name': name,
            'key': key    
        })

    def create_race(self, name, key, track, password, car_count):
        self._send('createRace', {
            'botId': {
                'name': name,
                'key': key
            },
            'trackName': track,
            'password': password,
            'carCount': car_count
        })

    def joinRace(self):
        # TODO
        raise NotImplementedError('')

    def ping(self, tick):
        self._send('ping', None, tick)

    def throttle(self, value, tick):
        if value < 0.0 or value > 1.0:
            raise Exception('Invalid throttle: %f' % value)

        self._send('throttle', value, tick)

    def switch_lane(self, side, tick):
        if side not in ['Left', 'Right']:
            raise Exception('Invalid lane option: %s', side)

        self._send('switchLane', side, tick)

    def turbo(self, msg, tick):
        # TODO
        raise NotImplementedError('')
