# encoding: utf-8

import re

from mangue.track import Track
from mangue.car import *

ANSWERABLE_MSGS = [
    'on_game_start',
    'on_car_positions'
]


class BaseBot(object):
    def __init__(self, name, key):
        self.protocol = None
        self.data = None

        self.started = False
        self.tick = None

        self.key = key
        self.name = name

        self.track = None
        self.car = None

        self.command_issued = False

    def set_protocol(self, protocol):
        if self.protocol:
            raise Exception('Already has protocol')

        self.protocol = protocol
        self.protocol.set_handler(self)

    @property
    def speed(self):
        return self.car.speed

    @property
    def piece(self):
        return self.car.piece

    @property
    def segment(self):
        return self.car.piece.segment

    @property
    def throttle(self):
        return self.car.throttle

    @throttle.setter
    def throttle(self, value):
        self.command_issued = True
        self.car.throttle = value
        self.protocol.throttle(value, self.tick)
        # print '<<< throttle', value

    @property
    def turbo(self):
        return self.car.turbo

    @turbo.setter
    def turbo(self, value):
        if value not in Turbo.__dict__.values():
            raise Exception('invalid turbo option: %s' % value)

        if value == Turbo.USE:
            if self.car.turbo != Turbo.AVAILABLE:
                raise Exception('turbo not available')

            self.car.turbo = Turbo.CHARGING
            self.command_issued = True
            self.protocol.turbo(None, self.tick)
            print '<<< turbo'
        else:
            self.car.turbo = value

    @property
    def lane(self):
        return self.car.end_lane

    @property
    def in_switch_piece(self):
        return self.car.piece.switch

    @property
    def switching(self):
        return self.car.start_lane != self.car.end_lane

    def switch_lane(self, to):
        self.command_issued = True
        self.protocol.switch_lane(to, self.tick)
        print '<<< switchlane', to

    def ping(self, ping=True):
        if ping:
            self.command_issued = True
            self.protocol.ping(self.tick)
            print '<<< ping'

    def on_create_race(self):
        print '>>> createrace'

    def on_join(self):
        print '>>> joined'

    def on_your_car(self):
        print '>>> yourcar'
        self.color = self.data['color']

    def on_game_init(self):
        print '>>> gameinit'
        self.track, self.car = Track.from_dict(self.data, self)

    def on_game_start(self):
        print '>>> gamestart'
        self.started = True

    def on_car_positions(self):
        # print '>>> carpositions'
        self.track.update_car_positions(self.data, self.tick)

    def on_turbo_available(self):
        print '>>> turboavailable'
        self.turbo = Turbo.AVAILABLE

    def on_game_end(self):
        print '>>> gameend'

    def on_tournament_end(self):
        print '>>> tournamentend'

    def on_crash(self):
        print '>>> crash'

    def on_spawn(self):
        print '>>> spawn'

    def on_lap_finished(self):
        print '>>> lapfinished'

    def on_dnf(self, ping=True):
        print '>>> dnf'

    def on_finish(self):
        print '>>> finish'

    def on_error(self):
        print '>>> error', self.data

    # --------------------

    def join_race(self):
        self.protocol.connect()
        self.protocol.join(self.name, self.key)

        self.loop()

    def create_solo_test_race(self, track, *args):
        kwargs = {
            'name': self.name,
            'key': self.key,
            'track': track,
            'password': 'manguebeat',
            'car_count': 1
        }

        self.protocol.connect()
        self.protocol.create_race(**kwargs)

        self.loop()

    def loop(self):
        try:
            while True:
                msg_type, data, tick = self.protocol.recv()

                if msg_type is None:
                    break

                self.data = data
                self.tick = tick
                self.command_issued = False

                func = 'on_' + re.sub('([A-Z]+)', r'_\1', msg_type).lower()

                # Corrige o bug de receber um carPositions antes do gameInit
                if not self.started and func == 'on_car_positions':
                    BaseBot.on_car_positions(self)
                else:
                    getattr(self, func)()

                if not self.command_issued and func in ANSWERABLE_MSGS:
                    self.ping()
        except KeyboardInterrupt:
            print
        except ValueError:
            print
