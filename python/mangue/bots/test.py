# encoding: utf-8

from .manguebot import MangueBot
from mangue.screen import Screen

class TestBot(MangueBot):
    def __init__(self, *args, **kwargs):
        MangueBot.__init__(self, *args, **kwargs)
        self.screen = Screen()

    def on_game_init(self):
        MangueBot.on_game_init(self)
        self.screen.process_track(self.track)

    def on_car_positions(self):
        MangueBot.on_car_positions(self)

        # print self.car
        # print 'SPEED', self.car.speed, 'pixels/tick'
        # print 'TICKS', self.protocol.tick_rate, 'ticks/s'
        self.screen.draw(self.track)
