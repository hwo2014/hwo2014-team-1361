# encoding: utf-8

import math, sys
from mangue.piece import *

# __all__ = ['FindEnginePowerState', 'RunState']


class SwitchState:
    '''
    Máscara de estados do switch. Um switch pode estar acontecendo e o
    próximo já pode estar agendado.
    '''
    EMPTY           = 0
    SCHEDULED       = 1
    HAPPENING       = 2


class BotState(object):
    def __init__(self, bot):
        self.bot = bot

    def __getattr__(self, name):
        return getattr(self.bot, name)


class TestState(BotState):
    def __init__(self, *args, **kwargs):
        BotState.__init__(self, *args, **kwargs)
        self.ticks = 0

    def on_car_positions(self):
        if isinstance(self.car.piece, BendPiece) and self.car.dist > \
                self.car.piece.lane_length(self.car.start_lane) / 5:
            self.bot.throttle = 0.0
        else:
            self.bot.throttle = 1.0

        print self.bot.throttle, self.car.angle

        # if self.ticks < 300:
        #     self.bot.throttle = 0.30
        # elif self.ticks < 600:
        #     self.bot.throttle = 0.80
        # else:
        #     import sys
        #     sys.exit()

        # self.ticks += 1

class FindEnginePowerState(BotState):
    THROTTLE_STEP   = 0
    MEASURE_STEP    = 1

    def __init__(self, *args, **kwargs):
        BotState.__init__(self, *args, **kwargs)
        self.step = self.THROTTLE_STEP

    def on_car_positions(self):
        if self.step == self.THROTTLE_STEP:
            self.step = self.MEASURE_STEP

        elif self.step == self.MEASURE_STEP:
            engine_power = self.bot.speed / (
                self.bot.MAX_ACC * self.bot.throttle)
            self.bot.set_engine_power(engine_power)
            self.bot.change_state(RunState)

        self.bot.throttle = 1.0


class RunState(BotState):
    THROTTLE = [1.0, 0.9, 0.8, 0.7, 0.6, 0.5, 0.4, 0.3, 0.2, 0.1, 0.0]

    def __init__(self, *args, **kwargs):
        BotState.__init__(self, *args, **kwargs)
        self.switch_state = SwitchState.EMPTY

    def num_ticks(self):
        # return int(math.ceil(self.speed * 20) / (self.max_speed(1.0))) + 5
        return int(40 * self.engine_power)

    def on_car_positions(self):
        # --------------------------------------------------------------------
        # SWITCH LANE
        #
        # Atualmente mudamos para lane que oferece a menor distância até o
        # próximo segmento.
        #
        # Fazemos essa mudança sempre na peça anterior a primeira peça do
        # próximo segmento (que é um switch) porque depois de vários testes
        # parece ser o melhor lugar para fazê-lo (em outros lugares o comando
        # é ignorado as vezes).
        #
        # Também foi percebido que o comando de switch só é "computado" se o
        # carro tiver alguma velocidade, então só depois que o speed for maior
        # que zero que enviamos o comando de mudar de lane.
        # --------------------------------------------------------------------

        next_piece = self.bot.piece.next()
        switch_scheduled = bool(self.switch_state & SwitchState.SCHEDULED)

        if next_piece.switch and not switch_scheduled and self.speed > 0:
            target_lane = self.bot.segment.next().shortest_lane(self.bot.lane)

            if target_lane != self.bot.lane:
                switch_to = target_lane.switch_recipe(self.bot.lane)
                self.bot.switch_lane(switch_to)

            # Mesmo que já estejamos na melhor lane, marcamos como SCHEDULED
            # para eviar calcular o melhor switch novamente.
            self.switch_state |= SwitchState.SCHEDULED

        if self.bot.in_switch_piece:
            self.switch_state |= SwitchState.HAPPENING
            self.switch_state &= ~SwitchState.SCHEDULED
        else:
            self.switch_state &= ~SwitchState.HAPPENING

        # --------------------------------------------------------------------
        # TURBO
        # --------------------------------------------------------------------

        # TODO :(

        # --------------------------------------------------------------------
        # THROTTLE
        # --------------------------------------------------------------------

        if self.bot.command_issued:
            return

        for throttle in self.THROTTLE:
            s, angles = self.bot.predict(self.num_ticks(), throttle)

            print throttle, '%.2f %.1f' % (self.speed, self.car.angle),
            for a in angles:
                print '%.1f' % a,
            print
            sys.stdout.flush()

            if not len(filter(lambda e: abs(e) > 40.0, angles)):
                self.bot.throttle = throttle
                return

        print 'SHIT!'
        self.bot.throttle = 0.0
