# encoding: utf-8

import math

from .base import BaseBot
from .state import *


class MangueBot(BaseBot):
    def __init__(self, *args, **kwargs):
        BaseBot.__init__(self, *args, **kwargs)
        # self.change_state(TestState)
        self.change_state(FindEnginePowerState)

        self.engine_power = None
        self.MAX_ACC = 0.2

    def change_state(self, state):
        self.state = state(self)

    def on_car_positions(self):
        BaseBot.on_car_positions(self)
        self.state.on_car_positions()

    def set_engine_power(self, ep):
        self.engine_power = ep
        self.max_acc = self.MAX_ACC * self.engine_power

    def max_speed(self, throttle):
        return 10 * throttle * self.engine_power

    def calc_acc(self, throttle=None, speed=None):
        '''
        http://en.wikipedia.org/wiki/Exponential_decay
        a(v) = A_MAX * e^(-v/5.0)
        '''
        if throttle is None:
            throttle = self.throttle

        if throttle != self.throttle:
            throttle -= self.throttle

        if speed is None:
            speed = self.speed

        acc = self.max_acc * throttle * math.exp(-speed/5.0)

        # if speed > self.max_speed(throttle):
        #     acc *= -1

        return acc

    def calc_speed(self, ticks=1, throttle=None):
        '''
        v(t) = v(t-1) + a(v(t-1))
        '''
        if throttle is None:
            throttle = self.throttle

        speed = self.speed
        sl = []

        for tick in xrange(ticks):
            speed = speed + self.calc_acc(throttle, speed)
            sl.append(speed)

        return sl

    def calc_angle(self, speed, piece, angle, angle_, angle__):
        '''
        a(t) = a(t-1) + 0.98a'(t-1) + (0.9 + 0.2Fc)a''(t-1)
        '''
        if not piece or not piece.radius:
            Fc = 0
        else:
            Fc = (speed ** 2) / (piece.radius
                + piece.lane_radius(self.car.start_lane))

        a = angle + 0.98 * angle_ + (0.9 + 0.2 * Fc) * angle__

        if a == 0 and Fc > 0.3:
            a = 4.0 if piece.turn == 'right' else -4.0

        return a

    def predict(self, ticks=1, throttle=None):
        '''
        Calcula a velocidade e ângulo do carro previstos até o tick
        especificado.
        '''
        if throttle is None:
            throttle = self.throttle

        angle = [self.car.angle]
        angle_ = self.car.angle_
        angle__ = self.car.angle__

        piece = self.car.piece
        dist = self.car.dist
        lane = self.car.start_lane

        # Calcula as velocidades
        speed = self.calc_speed(ticks, throttle)

        for s in speed:
            # Onde o carro estará (peça e posição dentro da peça)
            dist += s
            pl = piece.lane_length(lane)
            if dist > pl:
                dist -= pl
                piece = piece.next()

            # Próximo ângulo
            next_angle = self.calc_angle(s, piece, angle[-1], angle_, angle__)
            next_angle_ = next_angle - angle[-1]
            angle__ = next_angle_ - angle_
            angle_ = next_angle_

            angle.append(next_angle)

        return speed, angle[1:]
