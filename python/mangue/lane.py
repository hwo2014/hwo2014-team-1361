# encoding: utf-8

class Lane(object):
    def __init__(self, dist, idx):
        self.dist = dist
        self.idx = idx

    @staticmethod
    def from_dict(data, idx):
        return Lane(data['distanceFromCenter'], idx)

    def switch_recipe(self, from_):
        if from_.dist < self.dist:
            return 'Right'
        else:
            return 'Left'

    def __repr__(self):
        return '<Lane: dist: %d>' % self.dist
