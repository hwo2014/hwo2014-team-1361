# encoding: utf-8

import math

from mangue.util import Bundle

__all__ = ['Path', 'Segment']


class Path(object):
    def __init__(self, segment, start_lane, end_lane):
        self.start_lane = start_lane
        self.end_lane = end_lane
        self.segment = segment

        self.length = 0

    def add_piece(self, piece):
        if not piece.switch:
            # O switch já foi feito, então o resto do Path acontece na lane
            # final.
            length = piece.lane_length(self.end_lane)
        if piece.switch:
            # FIXME: Estamos considerando que a distância percorrida em uma peça
            # enquanto troca de lane é igual hipotenusa do tamanho da peça na
            # lane inicial pela distância entre as lanes.
            diff = abs(self.start_lane.dist - self.end_lane.dist)
            length = piece.lane_length(self.start_lane)
            length = math.sqrt(length**2 + diff**2)

        self.length += length

    def __repr__(self):
        return '<Path segment: %d startlane: %d endlane: %d length: %d>' % (
            self.segment.idx, self.start_lane.idx, self.end_lane.idx,
            self.length)


class Segment(object):
    @staticmethod
    def set_track(track):
        Segment.track = track

    def __init__(self, idx):
        self.pieces = []
        self.length = 0
        self.idx = idx

        self.paths = []
        self.process_paths()

    def process_paths(self):
        for idx, lane in enumerate(self.track.lanes):
            current = lane
            paths = []

            if idx > 0:
                before = self.track.lanes[idx - 1]
                paths.append(Path(self, current, before))

            paths.append(Path(self, current, current))

            if idx < len(self.track.lanes) - 1:
                after = self.track.lanes[idx + 1]
                paths.append(Path(self, current, after))

            self.paths.extend(paths)

    def is_complete(self):
        return len(self.pieces) and self.pieces[0].switch

    def add_piece(self, piece):
        self.pieces.append(piece)
        self.length += piece.length

        for path in self.paths:
            path.add_piece(piece)

        piece.set_segment(self)

    def merge(self, other):
        self.length += other.length

        for piece in other.pieces:
            for path in self.paths:
                path.add_piece(piece)

        self.pieces = other.pieces.extend(self.pieces)

    def next(self):
        '''
        Retorna o próximo segmento.
        '''
        segs = self.track.segments
        return segs[(self.idx + 1) % len(segs)]

    def shortest_lane(self, lane):
        '''
        Busca o Path com a menor distância que possui a lane parâmetro
        como seu start_lane. Retorna a end_lane deste Path.

        No caso de empate, prioriza a mesma lane do parâmetro (a lane atual).
        '''
        shortest = None

        for path in self.paths:
            if path.start_lane == lane:
                if not shortest or shortest.length > path.length or (
                        shortest.length == path.length
                        and path.end_lane == lane):
                    shortest = path

        return shortest.end_lane

    def __repr__(self):
        return '<Segment %d length: %f>' % (self.idx, self.length)
