# encoding: utf-8

import math

__all__ = ['Piece', 'StraightPiece', 'BendPiece']


class Piece(object):
    @staticmethod
    def set_track(track):
        Piece.track = track

    def __init__(self, idx, switch, length):
        self.idx = idx
        self.switch = switch
        self.length = length

        self.radius = 0
        self.angle = 0

    def set_segment(self, segment):
        self.segment = segment

    def lane_length(self, lane=None):
        return self.length

    def next(self):
        pieces = self.track.pieces
        return pieces[(self.idx + 1) % len(pieces)]


class StraightPiece(Piece):
    def __repr__(self):
        return '<Straigth %d len: %f switch: %r>' % (self.idx, self.length, self.switch)

    @staticmethod
    def from_dict(data, idx):
        kwargs = {
            'length': data['length'],
            'switch': data.get('switch', False),
            'idx': idx
        }

        return StraightPiece(**kwargs)

class BendPiece(Piece):
    def __init__(self, radius, angle, turn, **kwargs):
        kwargs['length'] = math.radians(abs(angle)) * radius
        Piece.__init__(self, **kwargs)

        self.radius = radius
        self.angle = angle
        self.turn = turn

    @staticmethod
    def from_dict(data, idx):
        if data['angle'] >= 0.0:
            turn = 'right'
        else:
            turn = 'left'

        kwargs = {
            'radius': data['radius'],
            'angle': data['angle'],
            'turn': turn,
            'switch': data.get('switch', False),
            'idx': idx
        }

        return BendPiece(**kwargs)

    def lane_radius(self, lane):
        '''
        Retorna o "raio adicional" da lane. Uma lane pode estar mais distante
        (maior raio) ou mais perto (menor raio) da parte central de uma curva.

        A mesma lane pode estar mais próximo do centro em uma curva e mais
        distante do centro em outra curva, basta que estas curvas sejam em
        sentidos diferentes.

        Então para descobrir se devemos somar ou subtrair a distância da lane,
        olhamos para o sinal do ângulo da curva, pois o mesmo indica se a curva
        é no sentido horário (valor positivo) ou sentido anti-horário (valor
        negativo).
        '''
        return lane.dist * math.copysign(1, -self.angle)

    def lane_length(self, lane=None):
        if lane is None:
            return self.length

        angle = math.radians(abs(self.angle))
        radius = self.radius + self.lane_radius(lane)

        return angle * radius

    def __repr__(self):
        return '<Bend %d radius: %f angle: %f length: %f switch: %r>' % (
            self.idx, self.radius, self.angle, self.length, self.switch)
