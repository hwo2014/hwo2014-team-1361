# encoding: utf-8

import sys
from mangue.protocol import HWOProtocol

def build_bot(name, key, is_test):
    if is_test:
        from mangue.bots.test import TestBot
        return TestBot(name, key)
    else:
        from mangue.bots import MangueBot
        return MangueBot(name, key)

if __name__ == '__main__':
    if len(sys.argv) < 5:
        print('Usage: ./run host port botname botkey')
        sys.exit()

    host, port, name, key = sys.argv[1:5]
    sys.argv = sys.argv[5:]

    # Must be the last parameter
    is_test = '--test' in sys.argv
    if is_test:
        sys.argv = sys.argv[:-1]

    protocol = HWOProtocol(host, port)
    bot = build_bot(name, key, is_test)
    bot.set_protocol(protocol)

    if not is_test:
        bot.join_race()
    else:
        if not sys.argv:
            sys.argv.append('keimola')
        bot.create_solo_test_race(*sys.argv)
